Tomei Test

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:4000](http://localhost:4000) with your browser to see the result.

## Backend

To run the backend run the docker-compose in the folder backend-tomei

```bash
docker-compose up -d
```
