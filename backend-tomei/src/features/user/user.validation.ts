import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { UserService } from './user.service';

@Injectable()
export class UserValidation implements NestMiddleware {
  constructor(private readonly userService: UserService) {}
  async use(req: Request, res: Response, next: NextFunction) {
    if (!req.body.name)
      throw new HttpException('name is required', HttpStatus.BAD_REQUEST);

    if (!req.body.email)
      throw new HttpException('email is required', HttpStatus.BAD_REQUEST);

    if (!req.body.password)
      throw new HttpException('password is required', HttpStatus.BAD_REQUEST);

    if (!req.body.confirmPassword)
      throw new HttpException(
        'confirm password is required',
        HttpStatus.BAD_REQUEST,
      );

    if (req.body.password !== req.body.confirmPassword)
      throw new HttpException(
        'confirm password does not match with password',
        HttpStatus.BAD_REQUEST,
      );

    const emailRegex =
      /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/;
    const isEmailValid = emailRegex.test(req.body.email);

    if (!isEmailValid)
      throw new HttpException('email not valid', HttpStatus.BAD_REQUEST);

    const isEmailExists = await this.userService.findByEmail(req.body.email);
    if (isEmailExists)
      throw new HttpException('email existed', HttpStatus.BAD_REQUEST);

    next();
  }
}
