import { Inject, Injectable } from '@nestjs/common';
import { UserDto } from './user.dto';
import { User } from './user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @Inject('USERS_REPOSITORY')
    private usersRepository: typeof User,
  ) {}

  async findByEmail(email: string): Promise<User> {
    return this.usersRepository.findOne({ where: { email: email } });
  }

  async createUser(user: UserDto): Promise<User> {
    const pass = await bcrypt.hash(user.password, 10);
    return this.usersRepository.create({ ...user, password: pass });
  }
}
